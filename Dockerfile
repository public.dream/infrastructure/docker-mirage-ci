FROM ocaml/opam:debian-ocaml-4.11

RUN sudo apt-get update -y
RUN sudo apt-get install -y \
                         m4

ENV PATH="/home/opam/.local/bin:${PATH}"

# --- put extra ocaml modules here, and remember to update dot-ocaml-init.
RUN opam init
RUN opam install \
    mirage

ENV PATH="/home/opam/.opam/4.11/bin:${PATH}"
